package lv.kristaps.spaceapp.data.datasource

import io.reactivex.Flowable
import lv.kristaps.spaceapp.data.ArticleApi
import lv.kristaps.spaceapp.model.Article
import javax.inject.Inject

class RemoteArticleDataSource @Inject constructor(
    private val articleApi: ArticleApi
): ArticleDataSource {
    override fun getArticles(): Flowable<List<Article>> {
        return articleApi.getArticles()
    }

    companion object {
        const val DI_TAG = "remote"
    }
}