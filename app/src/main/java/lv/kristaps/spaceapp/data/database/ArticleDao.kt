package lv.kristaps.spaceapp.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable
import lv.kristaps.spaceapp.model.Article
import lv.kristaps.spaceapp.model.ENTITY_ARTICLE

@Dao
interface ArticleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(articles: List<Article>)

    @Query("SELECT * from $ENTITY_ARTICLE")
    fun getAll(): Flowable<List<Article>>
}