package lv.kristaps.spaceapp.data.datasource

import io.reactivex.Flowable
import io.reactivex.Single
import lv.kristaps.spaceapp.model.Article

interface ArticleDataSource {
    fun getArticles(): Flowable<List<Article>> = Flowable.just(listOf())
    fun saveArticles(articles: List<Article>): Single<List<Article>> = Single.just(listOf())
}