package lv.kristaps.spaceapp.data.datasource

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import lv.kristaps.spaceapp.data.database.ArticleDao
import lv.kristaps.spaceapp.model.Article
import javax.inject.Inject

class LocalArticleDataSource @Inject constructor(
        private val articleDao: ArticleDao
): ArticleDataSource {

    override fun getArticles(): Flowable<List<Article>> {
        return articleDao.getAll()
    }

    override fun saveArticles(articles: List<Article>): Single<List<Article>> {
        return Completable.fromAction {
            articleDao.insert(articles)
        }.andThen(Single.just(articles))
    }

    companion object {
        const val DI_TAG = "local"
    }
}