package lv.kristaps.spaceapp.data

import io.reactivex.Flowable
import lv.kristaps.spaceapp.model.Article
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticleApi {
    @GET("/api/v2/articles")
    fun getArticles(
        @Query("_limit") limit: Int? = 50
    ): Flowable<List<Article>>
}