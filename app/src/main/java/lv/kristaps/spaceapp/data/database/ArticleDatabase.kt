package lv.kristaps.spaceapp.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import lv.kristaps.spaceapp.model.Article

@Database(entities = [Article::class], version = 1)
abstract class ArticleDatabase : RoomDatabase() {
    abstract fun articleDao(): ArticleDao
}