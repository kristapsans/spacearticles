package lv.kristaps.spaceapp

const val API_BASE = "https://spaceflightnewsapi.net/"
const val DB_NAME = "spaceapp.db"
const val DEFAULT_TIMEOUT_SECONDS = 30L