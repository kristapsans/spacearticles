package lv.kristaps.spaceapp.adapter

import androidx.recyclerview.widget.DiffUtil
import lv.kristaps.spaceapp.model.Article

class MyDiffCallback(
    private val old: List<Article>,
    private val new: List<Article>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return old[oldItemPosition].id == new[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = old[oldItemPosition]
        val newItem = new[newItemPosition]
        return oldItem == newItem
    }

    override fun getOldListSize(): Int = old.size
    override fun getNewListSize(): Int = new.size
}