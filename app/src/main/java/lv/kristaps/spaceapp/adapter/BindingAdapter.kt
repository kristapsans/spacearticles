package lv.kristaps.spaceapp.adapter

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import lv.kristaps.spaceapp.extensions.load

@BindingAdapter("url")
fun setUrl(imageView: ImageView, url: String) {
    imageView.load(url)
}