package lv.kristaps.spaceapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import lv.kristaps.spaceapp.ui.ClickListener
import lv.kristaps.spaceapp.R
import lv.kristaps.spaceapp.databinding.ArticleListItemBinding
import lv.kristaps.spaceapp.model.Article
import lv.kristaps.spaceapp.view.ArticleViewHolder

class ArticleListAdapter(
    private val clickListener: ClickListener
) : RecyclerView.Adapter<ArticleViewHolder>() {

    private val articles: MutableList<Article> = mutableListOf()

    fun updateList(newArticles: List<Article>) {
        val result = DiffUtil.calculateDiff(MyDiffCallback(articles, newArticles))
        articles.apply {
            clear()
            addAll(newArticles)
        }
        result.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val binding: ArticleListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.article_list_item,
            parent,
            false
        )

        return ArticleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.articleListItemBinding.apply {
            article = articles[position]
            click = clickListener
        }
    }

    override fun getItemCount(): Int = articles.size
}