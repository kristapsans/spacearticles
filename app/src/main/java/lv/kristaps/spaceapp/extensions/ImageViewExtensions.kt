package lv.kristaps.spaceapp.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory

private fun getTransitionOptions(): DrawableTransitionOptions {
    val factory = DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build()
    return DrawableTransitionOptions.withCrossFade(factory)
}

fun ImageView.load(url: String) {
    Glide.with(this)
            .load(url)
            .transition(getTransitionOptions())
            .centerCrop()
            .into(this)
}