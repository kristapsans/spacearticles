package lv.kristaps.spaceapp.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

const val ENTITY_ARTICLE = "article"

@Entity(tableName = ENTITY_ARTICLE)
data class Article(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "url")
    val url: String,
    @ColumnInfo(name = "imageUrl")
    val imageUrl: String,
    @ColumnInfo(name = "newsSite")
    var newsSite: String,
    @ColumnInfo(name = "summary")
    val summary: String,
    @ColumnInfo(name = "publishedAt")
    val publishedAt: String,
    @ColumnInfo(name = "updatedAt")
    val updatedAt: String,
    @ColumnInfo(name = "featured")
    val featured: Boolean
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt() == 1
    ) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(id)
        dest?.writeString(title)
        dest?.writeString(url)
        dest?.writeString(imageUrl)
        dest?.writeString(newsSite)
        dest?.writeString(summary)
        dest?.writeString(publishedAt)
        dest?.writeString(updatedAt)
        dest?.writeInt(if (featured) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        super.equals(other)
        return other is Article
                && id == other.id
                && title == other.title
                && url == other.url
                && imageUrl == other.imageUrl
                && newsSite == other.newsSite
                && summary == other.summary
                && publishedAt == other.publishedAt
                && updatedAt == other.updatedAt
                && featured == other.featured
    }

    companion object CREATOR : Parcelable.Creator<Article> {
        override fun createFromParcel(parcel: Parcel): Article {
            return Article(parcel)
        }

        override fun newArray(size: Int): Array<Article?> {
            return arrayOfNulls(size)
        }
    }
}