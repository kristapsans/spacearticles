package lv.kristaps.spaceapp.exceptions

class ConnectivityIsDownException : Throwable("Connectivity is down")