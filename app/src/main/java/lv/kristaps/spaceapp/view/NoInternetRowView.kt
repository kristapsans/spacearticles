package lv.kristaps.spaceapp.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.view.isVisible
import lv.kristaps.spaceapp.R

class NoInternetRowView(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int
) : LinearLayout(context, attrs, defStyleAttr) {
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context) : this(context, null)

    init {
        LayoutInflater.from(context).inflate(R.layout.no_internet_row, this, true)
    }

    fun show() {
        isVisible = true
    }

    fun hide() {
        isVisible = false
    }
}