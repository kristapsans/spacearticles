package lv.kristaps.spaceapp.view

import androidx.recyclerview.widget.RecyclerView
import lv.kristaps.spaceapp.databinding.ArticleListItemBinding

class ArticleViewHolder(
    val articleListItemBinding: ArticleListItemBinding
) : RecyclerView.ViewHolder(articleListItemBinding.root)