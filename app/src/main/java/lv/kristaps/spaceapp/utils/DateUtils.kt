package lv.kristaps.spaceapp.utils

import java.text.SimpleDateFormat

private val serverDateFormat = "yyyy-MM-dd'T'HH:mm"
private val defaultDateFormat = "dd.MM.YYYY HH:ss"

fun String.formatDate(): String {
    return SimpleDateFormat(serverDateFormat).parse(this)?.let {
        SimpleDateFormat(defaultDateFormat).format(it)
    } ?: kotlin.run {
        ""
    }
}