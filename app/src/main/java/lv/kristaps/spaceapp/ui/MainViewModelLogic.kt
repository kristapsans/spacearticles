package lv.kristaps.spaceapp.ui

import lv.kristaps.spaceapp.model.Article

fun getViewState(articles: List<Article>): ViewState {
    return if (articles.isNotEmpty()) {
        ViewState.Loaded(articles)
    } else {
        ViewState.NoContent
    }
}