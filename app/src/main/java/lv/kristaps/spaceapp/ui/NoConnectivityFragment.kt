package lv.kristaps.spaceapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerFragment
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_no_connectivity.*
import lv.kristaps.spaceapp.R
import lv.kristaps.spaceapp.di.MainScheduler
import javax.inject.Inject

class NoConnectivityFragment : DaggerFragment() {

    @Inject
    @MainScheduler
    lateinit var mainScheduler: Scheduler

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProvider.Factory
    lateinit var viewModel: NoConnectivityViewModel
    private val subscriptions = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_no_connectivity, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelProviderFactory).get(NoConnectivityViewModel::class.java)
    }

    private fun observe() {
        viewModel.showObs
                .observeOn(mainScheduler)
                .subscribeBy(onNext = ::handleVisibilityChange)
                .addTo(subscriptions)
    }

    private fun handleVisibilityChange(show: Boolean) {
        if (isAdded) {
            if (show) {
                noConnectionView?.hide()
            } else {
                noConnectionView?.show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        observe()
    }

    override fun onStop() {
        subscriptions.clear()
        super.onStop()
    }

    companion object {
        const val FRAGMENT_TAG = "NoConnectivityFragment"

        @JvmStatic
        fun newInstance() = NoConnectivityFragment()
    }
}