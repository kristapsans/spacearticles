package lv.kristaps.spaceapp.ui

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import lv.kristaps.spaceapp.di.IOScheduler
import lv.kristaps.spaceapp.model.Article
import lv.kristaps.spaceapp.repository.ArticleRepository
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainViewModel @Inject constructor(
        private val articleRepository: ArticleRepository,
        @IOScheduler private val ioScheduler: Scheduler
) : BaseViewModel() {

    private val showArticleSubject: PublishSubject<Article> = PublishSubject.create()
    val showArticleObs: Observable<Article>
        get() = showArticleSubject
                .throttleFirst(1L, TimeUnit.SECONDS)

    private val viewStateSubject: BehaviorSubject<ViewState> = BehaviorSubject.createDefault(ViewState.Loading)
    val viewStateObs: Observable<ViewState>
        get() = viewStateSubject

    init {
        observeArticles()
        observeErrors()
    }

    private fun observeArticles() {
        articleRepository
            .articlesObs
            .subscribeOn(ioScheduler)
            .subscribeBy(
                onNext = {
                    viewStateSubject.onNext(getViewState(it))
                }
            ).addTo(subscriptions)
    }

    private fun observeErrors() {
        articleRepository
            .errorObs
            .subscribeOn(ioScheduler)
            .subscribeBy(
                onNext = {
                    viewStateSubject.onNext(ViewState.Error(getErrorMessageResId(it)))
                }
            ).addTo(subscriptions)
    }

    fun onArticleSelected(article: Article) {
        showArticleSubject.onNext(article)
    }
}