package lv.kristaps.spaceapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.details_fragment.*
import lv.kristaps.spaceapp.R
import lv.kristaps.spaceapp.extensions.load
import lv.kristaps.spaceapp.model.Article
import lv.kristaps.spaceapp.utils.formatDate

class DetailsFragment : DaggerFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (arguments?.getParcelable(PARAM_ARTICLE) as? Article)?.let {
            show(it)
        }
    }

    private fun show(article: Article) {
        with(article) {
            ivImage.load(imageUrl)
            tvTitle.text = title
            tvSite.text = newsSite
            tvSummary.text = summary
            tvPublishedAt.text = "Published at: ${publishedAt.formatDate()}"
            tvUpdatedAt.text = "Updated at: ${updatedAt.formatDate()}"
        }
    }

    companion object {
        fun newInstance() = DetailsFragment()
        const val PARAM_ARTICLE = "article"
    }
}