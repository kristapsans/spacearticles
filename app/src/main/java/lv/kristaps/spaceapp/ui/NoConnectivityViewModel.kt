package lv.kristaps.spaceapp.ui

import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import lv.kristaps.spaceapp.network.ConnectivityNotifier
import javax.inject.Inject

class NoConnectivityViewModel @Inject constructor(
        private val connectivityNotifier: ConnectivityNotifier
) : BaseViewModel() {

    private val showSubject: BehaviorSubject<Boolean> = BehaviorSubject.createDefault(false)
    val showObs: Observable<Boolean>
        get() = showSubject

    init {
        connectivityNotifier
                .observeConnectivity
                .subscribeBy(
                        onNext = { showSubject.onNext(it.isConnected) },
                        onError = { showSubject.onNext(false) }
                ).addTo(subscriptions)
    }
}