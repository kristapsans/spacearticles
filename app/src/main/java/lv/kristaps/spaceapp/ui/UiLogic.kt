package lv.kristaps.spaceapp.ui

import lv.kristaps.spaceapp.R
import lv.kristaps.spaceapp.exceptions.ConnectivityIsDownException

fun getErrorMessageResId(error: Throwable): Int {
    return when (error) {
        is ConnectivityIsDownException -> R.string.network_error
        else -> R.string.generic_error
    }
}