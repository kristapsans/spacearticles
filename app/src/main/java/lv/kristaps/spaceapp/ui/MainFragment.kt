package lv.kristaps.spaceapp.ui

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerFragment
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.main_fragment.*
import lv.kristaps.spaceapp.R
import lv.kristaps.spaceapp.adapter.ArticleListAdapter
import lv.kristaps.spaceapp.di.MainScheduler
import lv.kristaps.spaceapp.model.Article
import javax.inject.Inject

sealed class ViewState {
    object Loading : ViewState()
    object NoContent : ViewState()
    class Error(@StringRes val stringResId: Int) : ViewState()
    class Loaded<T>(val content: T) : ViewState()
}

class MainFragment : DaggerFragment(), ClickListener {

    @Inject
    @MainScheduler
    lateinit var mainScheduler: Scheduler

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProvider.Factory
    lateinit var viewModel: MainViewModel

    private val articleAdapter by lazy { ArticleListAdapter(this) }
    private val subscriptions by lazy { CompositeDisposable() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setUpRecyclerView()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelProviderFactory).get(MainViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        observe()
    }

    override fun onStop() {
        subscriptions.clear()
        super.onStop()
    }

    private fun setUpRecyclerView() {
        recyclerView.apply {
            itemAnimator = DefaultItemAnimator()
            setHasFixedSize(true)
            adapter = articleAdapter
        }
    }

    private fun observe() {
        viewModel.viewStateObs
            .observeOn(mainScheduler)
            .subscribeBy(onNext = ::handleViewState)
            .addTo(subscriptions)

        viewModel.showArticleObs
                .observeOn(mainScheduler)
                .subscribeBy(onNext = ::openArticle)
                .addTo(subscriptions)
    }

    private fun handleViewState(viewState: ViewState) {
        when (viewState) {
            is ViewState.Loading -> pbLoader.isVisible = true
            is ViewState.NoContent -> {
                tvNoContent.isVisible = true
                pbLoader.isVisible = false
                articleAdapter.updateList(listOf())
            }
            is ViewState.Error -> {
                Snackbar.make(
                        root,
                        viewState.stringResId,
                        Snackbar.LENGTH_SHORT
                ).show()
            }
            is ViewState.Loaded<*> -> {
                (viewState.content as? List<Article>)?.let {
                    articleAdapter.updateList(it)
                }

                tvNoContent.isVisible = false
                pbLoader.isVisible = false
            }
        }
    }

    private fun openArticle(article: Article) {
        val bundle = Bundle().apply {
            putParcelable(DetailsFragment.PARAM_ARTICLE, article)
        }
        findNavController(this).navigate(R.id.action_mainFragment_to_detailsFragment, bundle)
    }

    override fun onItemClick(article: Article) {
        viewModel.onArticleSelected(article)
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}