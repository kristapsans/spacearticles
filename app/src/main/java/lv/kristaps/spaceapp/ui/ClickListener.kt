package lv.kristaps.spaceapp.ui

import lv.kristaps.spaceapp.model.Article

interface ClickListener {
    fun onItemClick(article: Article)
}