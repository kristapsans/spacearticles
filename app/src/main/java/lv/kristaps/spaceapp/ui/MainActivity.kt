package lv.kristaps.spaceapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import lv.kristaps.spaceapp.R

class MainActivity : AppCompatActivity() {

    private val navController by lazy {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        navHostFragment.navController
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        setUpNavController()
        addNoConnectivityFragment()
    }

    private fun setUpNavController() {
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        if (!navController.navigateUp()){
            onBackPressed()
        }
        return navController.navigateUp()
    }

    private fun addNoConnectivityFragment() {
        supportFragmentManager.apply {
            findFragmentByTag(NoConnectivityFragment.FRAGMENT_TAG)?.let { return }

            if (!isFinishing) {
                beginTransaction().apply {
                    add(R.id.noConnectivityFragmentContainer, NoConnectivityFragment.newInstance(), NoConnectivityFragment.FRAGMENT_TAG).commit()
                }
            }
        }
    }
}