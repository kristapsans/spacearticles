package lv.kristaps.spaceapp.network

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ProcessLifecycleOwner
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

data class ConnectivityInfo(
    val isConnected: Boolean
)

interface ConnectivityNotifier {
    val observeConnectivity: Observable<ConnectivityInfo>
}

internal class ConnectivityNotifierImpl @Inject constructor(
    private val connectivityManager: ConnectivityManager
) : ConnectivityManager.NetworkCallback(), ConnectivityNotifier, LifecycleEventObserver {

    private val activeNetworks: MutableList<Network> = mutableListOf()

    private val connectivitySubject: BehaviorSubject<ConnectivityInfo> =
        BehaviorSubject.createDefault(ConnectivityInfo(false))

    init {
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    override val observeConnectivity: Observable<ConnectivityInfo>
        get() = connectivitySubject.distinctUntilChanged { f, s ->
            f == s
        }

    override fun onAvailable(network: Network) {
        super.onAvailable(network)
        if (activeNetworks.none { activeNetwork -> activeNetwork == network }) {
            activeNetworks.add(network)
        }
        emmitNetworkChanges(activeNetworks.isNotEmpty(), network)
    }

    override fun onLost(network: Network) {
        super.onLost(network)
        activeNetworks.removeAll { activeNetwork -> activeNetwork == network }
        emmitNetworkChanges(activeNetworks.isNotEmpty(), network)
    }

    override fun onUnavailable() {
        super.onUnavailable()
        emmitNetworkChanges(false)
    }

    private fun emmitNetworkChanges(
        isConnected: Boolean,
        network: Network? = null
    ) {
        if (network == null) {
            connectivitySubject.onNext(ConnectivityInfo(isConnected))
            return
        }

        connectivitySubject.onNext(ConnectivityInfo(isConnected = isConnected))
    }

    private fun getNetworkRequest() = NetworkRequest.Builder()
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        .addTransportType(NetworkCapabilities.TRANSPORT_ETHERNET)
        .build()

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_CREATE -> connectivityManager.registerNetworkCallback(getNetworkRequest(), this)
            Lifecycle.Event.ON_DESTROY -> connectivityManager.unregisterNetworkCallback(this)
            else -> Unit
        }
    }
}