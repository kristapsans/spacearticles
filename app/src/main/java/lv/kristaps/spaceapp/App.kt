package lv.kristaps.spaceapp

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import lv.kristaps.spaceapp.di.DaggerAppComponent

class App : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }
}