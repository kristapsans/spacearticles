package lv.kristaps.spaceapp.di

import android.content.Context
import android.net.ConnectivityManager
import dagger.Binds
import dagger.Module
import dagger.Provides
import lv.kristaps.spaceapp.network.ConnectivityNotifier
import lv.kristaps.spaceapp.network.ConnectivityNotifierImpl
import javax.inject.Singleton

@Module
internal abstract class NetworkModule {

    @Binds
    @Singleton
    internal abstract fun bindConnectivityNotifier(
        impl: ConnectivityNotifierImpl
    ): ConnectivityNotifier

    @Module
    companion object {

        @Provides
        @Singleton
        @JvmStatic
        fun provideConnectivityManager(
            context: Context
        ): ConnectivityManager {
            return context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        }
    }
}