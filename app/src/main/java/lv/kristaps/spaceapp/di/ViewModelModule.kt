package lv.kristaps.spaceapp.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import lv.kristaps.spaceapp.ui.MainViewModel
import lv.kristaps.spaceapp.ui.NoConnectivityViewModel

@Module
internal abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: DaggerViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NoConnectivityViewModel::class)
    abstract fun bindNoConnectivityViewModel(viewModel: NoConnectivityViewModel): ViewModel
}