package lv.kristaps.spaceapp.di

import dagger.Module
import dagger.Provides
import lv.kristaps.spaceapp.API_BASE
import lv.kristaps.spaceapp.network.HttpClientHelper
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class BaseApiAddress

@Module
abstract class RetrofitModule {

    @Module
    companion object {

        @Provides
        @BaseApiAddress
        @JvmStatic
        fun provideApiBaseAddress(): String {
            return API_BASE
        }

        @Provides
        @Singleton
        @JvmStatic
        fun provideHttpClient(): OkHttpClient {
            return HttpClientHelper.getOkHttpClient()
        }

        @Provides
        @Singleton
        @JvmStatic
        fun provideRetrofit(
            httpClient: OkHttpClient,
            @BaseApiAddress baseApiAddress: String
        ): Retrofit {
            return Retrofit.Builder()
                    .client(httpClient)
                    .baseUrl(baseApiAddress)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
        }
    }
}