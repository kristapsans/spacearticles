package lv.kristaps.spaceapp.di

import android.content.Context
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import lv.kristaps.spaceapp.App

@Module(
    includes = [
        RepositoryModule::class,
        ViewModelModule::class
    ]
)
class AppModule {
    @Provides
    fun providesContext(app: App): Context {
        return app.applicationContext
    }

    @Module
    companion object {
        @Provides
        @MainScheduler
        @JvmStatic
        fun provideMainScheduler(): Scheduler = AndroidSchedulers.mainThread()

        @Provides
        @IOScheduler
        @JvmStatic
        fun provideIOScheduler(): Scheduler = Schedulers.io()
    }
}