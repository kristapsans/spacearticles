package lv.kristaps.spaceapp.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import lv.kristaps.spaceapp.ui.DetailsFragment
import lv.kristaps.spaceapp.ui.MainFragment
import lv.kristaps.spaceapp.ui.NoConnectivityFragment

@Module
abstract class ContributorModule {
    @ContributesAndroidInjector
    abstract fun contributeMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailsFragment(): DetailsFragment

    @ContributesAndroidInjector
    abstract fun contributeNoConnectivityFragment(): NoConnectivityFragment
}