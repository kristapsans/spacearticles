package lv.kristaps.spaceapp.di

import dagger.Binds
import dagger.Module
import lv.kristaps.spaceapp.repository.ArticleRepository
import lv.kristaps.spaceapp.repository.ArticleRepositoryImpl
import javax.inject.Singleton

@Module(includes = [DataSourceModule::class, NetworkModule::class])
internal abstract class RepositoryModule {
    @Binds
    @Singleton
    internal abstract fun bindArticleRepository(
        impl: ArticleRepositoryImpl
    ): ArticleRepository
}