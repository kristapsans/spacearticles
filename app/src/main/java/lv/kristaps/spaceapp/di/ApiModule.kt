package lv.kristaps.spaceapp.di

import dagger.Module
import dagger.Provides
import lv.kristaps.spaceapp.data.ArticleApi
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [RetrofitModule::class])
abstract class ApiModule {

    @Module
    companion object {

        @Provides
        @Singleton
        @JvmStatic
        fun provideArticleApi(
            retrofit: Retrofit
        ): ArticleApi {
            return retrofit.create(ArticleApi::class.java)
        }
    }
}