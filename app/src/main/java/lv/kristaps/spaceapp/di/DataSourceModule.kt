package lv.kristaps.spaceapp.di

import dagger.Binds
import dagger.Module
import lv.kristaps.spaceapp.data.datasource.ArticleDataSource
import lv.kristaps.spaceapp.data.datasource.LocalArticleDataSource
import lv.kristaps.spaceapp.data.datasource.RemoteArticleDataSource
import javax.inject.Named
import javax.inject.Singleton

@Module(
    includes = [
        ApiModule::class,
        DatabaseModule::class
    ]
)
abstract class DataSourceModule {

    @Binds
    @Singleton
    @Named(RemoteArticleDataSource.DI_TAG)
    internal abstract fun bindArticleRemoteDataSource(
        articleRemoteDataSource: RemoteArticleDataSource
    ): ArticleDataSource

    @Binds
    @Singleton
    @Named(LocalArticleDataSource.DI_TAG)
    internal abstract fun bindArticleLocalDataSource(
        articleLocalDataSource: LocalArticleDataSource
    ): ArticleDataSource
}