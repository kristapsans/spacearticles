package lv.kristaps.spaceapp.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import lv.kristaps.spaceapp.DB_NAME
import lv.kristaps.spaceapp.data.database.ArticleDao
import lv.kristaps.spaceapp.data.database.ArticleDatabase
import javax.inject.Singleton

@Module
internal abstract class DatabaseModule {

    @Module
    companion object {

        @Provides
        @Singleton
        @JvmStatic
        fun provideDatabase(context: Context): ArticleDatabase {
            return Room.databaseBuilder(context, ArticleDatabase::class.java, DB_NAME).build()
        }

        @Provides
        @Singleton
        @JvmStatic
        fun provideArticleDao(
            database: ArticleDatabase
        ): ArticleDao {
            return database.articleDao()
        }
    }
}