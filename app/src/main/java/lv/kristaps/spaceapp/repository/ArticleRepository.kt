package lv.kristaps.spaceapp.repository

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ProcessLifecycleOwner
import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import lv.kristaps.spaceapp.data.datasource.ArticleDataSource
import lv.kristaps.spaceapp.data.datasource.LocalArticleDataSource
import lv.kristaps.spaceapp.data.datasource.RemoteArticleDataSource
import lv.kristaps.spaceapp.di.IOScheduler
import lv.kristaps.spaceapp.exceptions.ConnectivityIsDownException
import lv.kristaps.spaceapp.model.Article
import lv.kristaps.spaceapp.network.ConnectivityInfo
import lv.kristaps.spaceapp.network.ConnectivityNotifier
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

interface ArticleRepository {
    val errorObs: Observable<Throwable>
    val articlesObs: Observable<List<Article>>
    val lastUpdateObs: Observable<Long>
}

class ArticleRepositoryImpl @Inject constructor(
        @Named(RemoteArticleDataSource.DI_TAG) private val remoteDataSource: ArticleDataSource,
        @Named(LocalArticleDataSource.DI_TAG) private val localDataSource: ArticleDataSource,
        @IOScheduler private val ioScheduler: Scheduler,
        private val connectivityNotifier: ConnectivityNotifier
) : ArticleRepository, LifecycleEventObserver {

    private val REFRESH_INTERVAL_IN_SECONDS = 10L
    private val RETRY_INTERVAL_IN_SECONDS = 3L

    private val subscriptions = CompositeDisposable()
    private var refreshTimer = Single.timer(REFRESH_INTERVAL_IN_SECONDS, TimeUnit.SECONDS)
    private var retryTimer = Maybe.timer(RETRY_INTERVAL_IN_SECONDS, TimeUnit.SECONDS)

    private val errorSubject: PublishSubject<Throwable> = PublishSubject.create()
    override val errorObs: Observable<Throwable>
        get() = errorSubject

    private val lastUpdateSubject: BehaviorSubject<Long> = BehaviorSubject.create()
    override val lastUpdateObs: Observable<Long>
        get() = lastUpdateSubject

    private val articlesSubject: BehaviorSubject<List<Article>> = BehaviorSubject.create()
    override val articlesObs: Observable<List<Article>>
        get() = articlesSubject

    init {
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    private fun getArticlesFromApiAndSave(): Single<List<Article>> {
        return remoteDataSource
                .getArticles()
                .firstOrError()
                .flatMap {
                    Log.d(TAG, "Got ${it.size} articles from API")
                    localDataSource.saveArticles(it)
                }
    }

    private fun observeLocalRepository() {
        localDataSource
                .getArticles()
                .subscribeOn(ioScheduler)
                .subscribeBy(
                        onNext = {
                            Log.d(TAG, "Got ${it.size} articles from DB")
                            articlesSubject.onNext(it)
                        }
                ).addTo(subscriptions)
    }

    private fun fetchArticlesPeriodically() {
        Log.d(TAG, "Fetch articles periodically every $REFRESH_INTERVAL_IN_SECONDS seconds")
            observeConnectivity()
            .flatMap { getArticlesFromApiAndSave() }
            .subscribeOn(ioScheduler)
            .repeatWhen { it.flatMapSingle { refreshTimer } }
            .retryWhen {
                it.flatMapMaybe { error ->
                    handleError(error)
                        .andThen(
                            Maybe.defer {
                                when (error) {
                                    is ConnectivityIsDownException -> observeIsConnectedState()
                                    else -> retryTimer.doOnSuccess { Log.d(TAG, "Trying again") }
                                }
                            }
                        )
                }
            }
            .subscribe().addTo(subscriptions)
    }

    private fun handleError(error: Throwable): Completable =
        Completable.fromAction {
            Log.e(TAG, error.message ?: "No message", null)
            errorSubject.onNext(error)
        }

    private fun observeIsConnectedState(): Maybe<ConnectivityInfo> {
        return connectivityNotifier.observeConnectivity
            .doOnSubscribe { Log.d(TAG, "Connection lost") }
            .filter { info -> info.isConnected }
            .doOnNext { Log.d(TAG, "Connection returned") }
            .firstElement()
    }

    private fun checkConnection(connectivityInfo: ConnectivityInfo): Single<Unit> {
        return if (connectivityInfo.isConnected) {
            Single.just(Unit)
        } else {
            Single.error(ConnectivityIsDownException())
        }
    }

    private fun observeConnectivity(): Single<Unit> {
        return connectivityNotifier
            .observeConnectivity
            .firstElement()
            .flatMapSingle(::checkConnection)
    }

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_START -> {
                fetchArticlesPeriodically()
                observeLocalRepository()
            }
            Lifecycle.Event.ON_STOP -> subscriptions.clear()
            else -> Unit
        }
    }

    companion object {
        private val TAG = ArticleRepository::class.java.simpleName
    }
}