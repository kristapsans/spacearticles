package lv.kristaps.spaceapp.ui.main

import lv.kristaps.spaceapp.R
import lv.kristaps.spaceapp.exceptions.ConnectivityIsDownException
import lv.kristaps.spaceapp.ui.getErrorMessageResId
import org.junit.Assert
import org.junit.Test

class UiLogicTest {

    @Test
    fun testErrorMessageResIds() {
        Assert.assertEquals(getErrorMessageResId(ConnectivityIsDownException()), R.string.network_error)
        Assert.assertEquals(getErrorMessageResId(Throwable("Error")), R.string.generic_error)
    }
}