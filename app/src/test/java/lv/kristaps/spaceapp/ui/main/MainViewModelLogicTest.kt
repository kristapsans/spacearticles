package lv.kristaps.spaceapp.ui.main

import lv.kristaps.spaceapp.model.Article
import lv.kristaps.spaceapp.ui.ViewState
import lv.kristaps.spaceapp.ui.getViewState
import org.junit.Assert.assertThat
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Test

class MainViewModelLogicTest {

    private val article = Article(
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        false)

    @Test
    fun testGetViewState() {
        val one = listOf(article)
        val multiple = listOf(article, article, article)
        val empty = emptyList<Article>()

        assertThat(getViewState(one), instanceOf(ViewState.Loaded::class.java))
        assertThat(getViewState(multiple), instanceOf(ViewState.Loaded::class.java))
        assertThat(getViewState(empty), instanceOf(ViewState.NoContent::class.java))
    }
}